/*
  Задание: Модуль создания плейлиста, используя паттерн Обсервер.

  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:
    1. Список исполнителей и песен (Находится слева) - отуда можно включить
    песню в исполнение иди добавить в плейлист.
    Если песня уже есть в плейлисте, дважды добавить её нельзя.

    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,
    или запустить в исполнение. Внизу списка должен выводиться блок, в котором
    пишет суммарное время проигрывания всех песен в плейлисте.

    3. Отображает песню которая проигрывается.

    4. + Бонус: Сделать прогресс пар того как проигрывается песня
    с возможностью его остановки.
*/

import { Observable, Observer } from '../observer/Observer';

const MusicList = [
  {
    title: 'Rammstain',
    songs: [
      {
        id: 1,
        name: 'Du Hast',
        time: [3, 12]
      },
      {
        id: 2,
        name: 'Ich Will',
        time: [5, 1]
      },
      {
        id: 3,
        name: 'Mutter',
        time: [4, 15]
      },
      {
        id: 4,
        name: 'Ich tu dir weh',
        time: [5, 13]
      },
      {
        id: 5,
        name: 'Rammstain',
        time: [3, 64]
      }
    ]
  },
  {
    title: 'System of a Down',
    songs: [
      {
        id: 6,
        name: 'Toxicity',
        time: [4, 22]
      },
      {
        id: 7,
        name: 'Sugar',
        time: [2, 44]
      },
      {
        id: 8,
        name: 'Lonely Day',
        time: [3, 19]
      },
      {
        id: 9,
        name: 'Lost in Hollywood',
        time: [5, 9]
      },
      {
        id: 10,
        name: 'Chop Suey!',
        time: [2, 57]
      }
    ]
  },
  {
    title: 'Green Day',
    songs: [
      {
        id: 11,
        name: '21 Guns',
        time: [4, 16]
      },
      {
        id: 12,
        name: 'Boulevard of broken dreams!',
        time: [6, 37]
      },
      {
        id: 13,
        name: 'Basket Case!',
        time: [3, 21]
      },
      {
        id: 14,
        name: 'Know Your Enemy',
        time: [4, 11]
      }
    ]
  },
  {
    title: 'Linkin Park',
    songs: [
      {
        id: 15,
        name: 'Numb',
        time: [3, 11]
      },
      {
        id: 16,
        name: 'New Divide',
        time: [4, 41]
      },
      {
        id: 17,
        name: 'Breaking the Habit',
        time: [4, 1]
      },
      {
        id: 18,
        name: 'Faint',
        time: [3, 29]
      }
    ]
  }
]

const MusicPlayList = document.getElementById('MusicPlayList');
const MusicListNode = document.getElementById('MusicBox');
const song__name = document.getElementsByClassName('song__name');
const song__creator = document.getElementsByClassName('song__creator');
const song__duration = document.getElementsByClassName('song__duration');

let observable = new Observable();
let observable2 = new Observable();
let observable3 = new Observable();

let currentSong = {};

const FindSongById = (id) => {
  let mySong;
  for (let i =0; i<MusicList.length; i++){
          
    mySong = MusicList[i].songs.find( item => {
      return item.id == id;
    })

    if (mySong){
      mySong.title = MusicList[i].title
      break;
    }
  }
  return mySong;
}

const DrawSongs = (array) => {
  MusicPlayList.innerHTML = "";
  array.forEach( item => {
    let divForSong = document.createElement("div");
      MusicPlayList.appendChild(divForSong);
    let template = `
      <button class="song_decor song_pl" data-id="${item.id}">&#x266b</button>
      <button class="song_decor song_del" data-id="${item.id}">&#x2718</button>
      <p class="song_decor">${array.indexOf(item)+1}. ${item.title} - ${item.name} (${item.time[0]}:${item.time[1]})</p>
    `;
    divForSong.innerHTML = template;
  })
  let btn_play = document.querySelectorAll(".song_pl");
  btn_play.forEach( (item) => {
    item.addEventListener('click', (e) => {
     observable2.sendMessage(e.target.getAttribute("data-id"));
    })
  })

  let btn_delete = document.querySelectorAll(".song_del");
  btn_delete.forEach( (item) => {
    item.addEventListener('click', (e) => {
     observable3.sendMessage(e.target.getAttribute("data-id"));
    })
  })
}


const MusicBox = () => {
  let myPlaylist = [];

  let playList = new Observer( id => { 
    let mySong = FindSongById(id);
    if (myPlaylist.indexOf( mySong ) == -1){
      myPlaylist.push(mySong);
      DrawSongs(myPlaylist);
    }
  });

  let musicPlaying = new Observer ( id => {
    currentSong = FindSongById(id);
    song__creator[0].innerHTML = currentSong.title;
    song__name[0].innerHTML = currentSong.name;
    song__duration[0].innerHTML = `${currentSong.time[0]}:${currentSong.time[1]}`;
  })

  let musicDelete = new Observer ( id => {
    let mySong = FindSongById(id);
    if (currentSong == mySong ){
      song__creator[0].innerHTML = "";
      song__name[0].innerHTML = "";
        song__duration[0].innerHTML = "";
    }
    myPlaylist.splice(myPlaylist.indexOf(myPlaylist.find( (item) => {
        return item.id == id;
    })),1);

    DrawSongs(myPlaylist);
  })

  observable.addObserver( playList );
  observable2.addObserver( musicPlaying );
  observable3.addObserver( musicDelete );

  MusicList.map( Artist => {
    let template = `
      <h3>${Artist.title}</h3>
      <ul>
    `;
    let node = document.createElement ("div");
         MusicListNode.appendChild(node);

    Artist.songs.map( Song => {
        template = template + `
          <button class="song_decor song_add" data-id="${Song.id}">+</button>
          <button class="song_decor song_from_musBox" data-id="${Song.id}">&#x266b</button>
          <li class="song_decor">${Song.name}</li>
          <br>
        `
     })

     template = template + `</ul>`;
     node.innerHTML = template;
  });
  
  let btn_add = document.querySelectorAll(".song_add");
  btn_add.forEach( (item) => {
    item.addEventListener('click', (e) => {
     observable.sendMessage(e.target.getAttribute("data-id"));
    })
  })

  let btn_pl_from_musBox = document.querySelectorAll(".song_from_musBox");
  btn_pl_from_musBox.forEach( (item) => {
    item.addEventListener('click', (e) => {
     observable2.sendMessage(e.target.getAttribute("data-id"));
    })
  })
}


export default MusicBox;
