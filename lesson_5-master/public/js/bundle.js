/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/classwork/customEvent.js":
/*!**********************************************!*\
  !*** ./application/classwork/customEvent.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в\n            соответсвии с правилавми ниже:\n\n  1. Написать кастомные события которые будут менять статус светофора:\n  - start: включает зеленый свет\n  - stop: включает красный свет\n  - night: включает желтый свет, который моргает с интервалом в 1с.\n  И зарегистрировать каждое через addEventListener на каждом из светофоров.\n\n  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,\n      чтобы включить режим \"нерегулируемого перекрестка\" (моргающий желтый).\n\n  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)\n      или зеленый (на второй клик) цвет соотвественно.\n      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.\n\n  4.  + Бонус: На кнопку \"Start Night\" повесить сброс всех светофоров с их текущего\n      статуса, на мигающий желтые.\n      Двойной, тройной и более клики на кнопку не должны вызывать повторную\n      инициализацию инвервала.\n\n*/\n\nconst roadTraffic = () => {\n  let trafficLights = document.querySelectorAll('.trafficLight');\n  let btn = document.getElementById(\"Do\");\n\n  let nightEvent = new CustomEvent(\"startYellow\");\n  let startNight = new CustomEvent(\"startNight\");\n\n  document.addEventListener(\"DOMContentLoaded\", () => {\n    trafficLights.forEach( (item) => {\n      item.dispatchEvent(nightEvent);\n    })\n  });\n\n  trafficLights.forEach( (item) => {\n    item.addEventListener('startYellow', function(e){\n      let timer = () => {\n        item.addEventListener('startNight' , () => {\n          item.classList.remove('yellow');\n          clearInterval(MyInterval);\n        })\n        \n        if(item.classList.contains('red') == false && item.classList.contains('green') == false){\n            item.classList.toggle('yellow');\n         } else {\n          clearInterval(MyInterval)\n         }\n      }\n      let MyInterval = setInterval(timer,1000); \n    })\n  })\n\n  trafficLights.forEach( (item) => {\n    item.addEventListener('click', (e) => {\n      if (item.classList.contains('red') == false){\n        console.log(\"Event\" , e);\n        let stopEvent = new CustomEvent('startRed',{detail: {\n          id : e.target.getAttribute(\"id\")\n            }\n        });\n        e.target.dispatchEvent(stopEvent);\n      \n     }else {\n        let startEvent = new CustomEvent('startGreen',{detail: {\n            id : e.target.getAttribute(\"id\")\n          }\n        });\n        e.target.dispatchEvent(startEvent);\n      } \n    })\n  })\n\n  trafficLights.forEach( (item) => {\n    item.addEventListener('startRed', function(event){\n          console.log(event);\n          let {id} = event.detail;\n          let currentLight = document.getElementById(id);\n          \n          if (currentLight.classList.contains('yellow')){\n            currentLight.classList.remove('yellow');\n          }\n\n          if (currentLight.classList.contains('green')){\n            currentLight.classList.remove('green');\n          }\n\n          currentLight.classList.add('red');\n        })\n\n    item.addEventListener('startGreen', function(){\n          let {id} = event.detail;\n          let currentLight = document.getElementById(id);\n          currentLight.classList.remove('red');\n          currentLight.classList.add('green');\n        })\n   })\n\n  btn.addEventListener('click' , () => {\n    trafficLights.forEach( item => {\n      item.classList.remove('red','green');\n      item.dispatchEvent(startNight);\n      item.dispatchEvent(nightEvent);\n    }) \n  })\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (roadTraffic);\n\n//# sourceURL=webpack:///./application/classwork/customEvent.js?");

/***/ }),

/***/ "./application/classwork/observer.js":
/*!*******************************************!*\
  !*** ./application/classwork/observer.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _observer_Observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../observer/Observer */ \"./application/observer/Observer.js\");\n/*\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\n\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\n    песню в исполнение иди добавить в плейлист.\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\n\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\n    пишет суммарное время проигрывания всех песен в плейлисте.\n\n    3. Отображает песню которая проигрывается.\n\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\n    с возможностью его остановки.\n*/\n\n\n\nconst MusicList = [\n  {\n    title: 'Rammstain',\n    songs: [\n      {\n        id: 1,\n        name: 'Du Hast',\n        time: [3, 12]\n      },\n      {\n        id: 2,\n        name: 'Ich Will',\n        time: [5, 1]\n      },\n      {\n        id: 3,\n        name: 'Mutter',\n        time: [4, 15]\n      },\n      {\n        id: 4,\n        name: 'Ich tu dir weh',\n        time: [5, 13]\n      },\n      {\n        id: 5,\n        name: 'Rammstain',\n        time: [3, 64]\n      }\n    ]\n  },\n  {\n    title: 'System of a Down',\n    songs: [\n      {\n        id: 6,\n        name: 'Toxicity',\n        time: [4, 22]\n      },\n      {\n        id: 7,\n        name: 'Sugar',\n        time: [2, 44]\n      },\n      {\n        id: 8,\n        name: 'Lonely Day',\n        time: [3, 19]\n      },\n      {\n        id: 9,\n        name: 'Lost in Hollywood',\n        time: [5, 9]\n      },\n      {\n        id: 10,\n        name: 'Chop Suey!',\n        time: [2, 57]\n      }\n    ]\n  },\n  {\n    title: 'Green Day',\n    songs: [\n      {\n        id: 11,\n        name: '21 Guns',\n        time: [4, 16]\n      },\n      {\n        id: 12,\n        name: 'Boulevard of broken dreams!',\n        time: [6, 37]\n      },\n      {\n        id: 13,\n        name: 'Basket Case!',\n        time: [3, 21]\n      },\n      {\n        id: 14,\n        name: 'Know Your Enemy',\n        time: [4, 11]\n      }\n    ]\n  },\n  {\n    title: 'Linkin Park',\n    songs: [\n      {\n        id: 15,\n        name: 'Numb',\n        time: [3, 11]\n      },\n      {\n        id: 16,\n        name: 'New Divide',\n        time: [4, 41]\n      },\n      {\n        id: 17,\n        name: 'Breaking the Habit',\n        time: [4, 1]\n      },\n      {\n        id: 18,\n        name: 'Faint',\n        time: [3, 29]\n      }\n    ]\n  }\n]\n\nconst MusicPlayList = document.getElementById('MusicPlayList');\nconst MusicListNode = document.getElementById('MusicBox');\nconst song__name = document.getElementsByClassName('song__name');\nconst song__creator = document.getElementsByClassName('song__creator');\nconst song__duration = document.getElementsByClassName('song__duration');\n\nlet observable = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\nlet observable2 = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\nlet observable3 = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observable\"]();\n\nlet currentSong = {};\n\nconst FindSongById = (id) => {\n  let mySong;\n  for (let i =0; i<MusicList.length; i++){\n          \n    mySong = MusicList[i].songs.find( item => {\n      return item.id == id;\n    })\n\n    if (mySong){\n      mySong.title = MusicList[i].title\n      break;\n    }\n  }\n  return mySong;\n}\n\nconst DrawSongs = (array) => {\n  MusicPlayList.innerHTML = \"\";\n  array.forEach( item => {\n    let divForSong = document.createElement(\"div\");\n      MusicPlayList.appendChild(divForSong);\n    let template = `\n      <button class=\"song_decor song_pl\" data-id=\"${item.id}\">&#x266b</button>\n      <button class=\"song_decor song_del\" data-id=\"${item.id}\">&#x2718</button>\n      <p class=\"song_decor\">${array.indexOf(item)+1}. ${item.title} - ${item.name} (${item.time[0]}:${item.time[1]})</p>\n    `;\n    divForSong.innerHTML = template;\n  })\n  let btn_play = document.querySelectorAll(\".song_pl\");\n  btn_play.forEach( (item) => {\n    item.addEventListener('click', (e) => {\n     observable2.sendMessage(e.target.getAttribute(\"data-id\"));\n    })\n  })\n\n  let btn_delete = document.querySelectorAll(\".song_del\");\n  btn_delete.forEach( (item) => {\n    item.addEventListener('click', (e) => {\n     observable3.sendMessage(e.target.getAttribute(\"data-id\"));\n    })\n  })\n}\n\n\nconst MusicBox = () => {\n  let myPlaylist = [];\n\n  let playList = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"]( id => { \n    let mySong = FindSongById(id);\n    if (myPlaylist.indexOf( mySong ) == -1){\n      myPlaylist.push(mySong);\n      DrawSongs(myPlaylist);\n    }\n  });\n\n  let musicPlaying = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"] ( id => {\n    currentSong = FindSongById(id);\n    song__creator[0].innerHTML = currentSong.title;\n    song__name[0].innerHTML = currentSong.name;\n    song__duration[0].innerHTML = `${currentSong.time[0]}:${currentSong.time[1]}`;\n  })\n\n  let musicDelete = new _observer_Observer__WEBPACK_IMPORTED_MODULE_0__[\"Observer\"] ( id => {\n    let mySong = FindSongById(id);\n    if (currentSong == mySong ){\n      song__creator[0].innerHTML = \"\";\n      song__name[0].innerHTML = \"\";\n        song__duration[0].innerHTML = \"\";\n    }\n    myPlaylist.splice(myPlaylist.indexOf(myPlaylist.find( (item) => {\n        return item.id == id;\n    })),1);\n\n    DrawSongs(myPlaylist);\n  })\n\n  observable.addObserver( playList );\n  observable2.addObserver( musicPlaying );\n  observable3.addObserver( musicDelete );\n\n  MusicList.map( Artist => {\n    let template = `\n      <h3>${Artist.title}</h3>\n      <ul>\n    `;\n    let node = document.createElement (\"div\");\n         MusicListNode.appendChild(node);\n\n    Artist.songs.map( Song => {\n        template = template + `\n          <button class=\"song_decor song_add\" data-id=\"${Song.id}\">+</button>\n          <button class=\"song_decor song_from_musBox\" data-id=\"${Song.id}\">&#x266b</button>\n          <li class=\"song_decor\">${Song.name}</li>\n          <br>\n        `\n     })\n\n     template = template + `</ul>`;\n     node.innerHTML = template;\n  });\n  \n  let btn_add = document.querySelectorAll(\".song_add\");\n  btn_add.forEach( (item) => {\n    item.addEventListener('click', (e) => {\n     observable.sendMessage(e.target.getAttribute(\"data-id\"));\n    })\n  })\n\n  let btn_pl_from_musBox = document.querySelectorAll(\".song_from_musBox\");\n  btn_pl_from_musBox.forEach( (item) => {\n    item.addEventListener('click', (e) => {\n     observable2.sendMessage(e.target.getAttribute(\"data-id\"));\n    })\n  })\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\n\n\n//# sourceURL=webpack:///./application/classwork/observer.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classwork/observer */ \"./application/classwork/observer.js\");\n/* harmony import */ var _classwork_customEvent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classwork/customEvent */ \"./application/classwork/customEvent.js\");\n\n//MusicBox()\n\n\n\nObject(_classwork_customEvent__WEBPACK_IMPORTED_MODULE_1__[\"default\"])()\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer/Observer.js":
/*!******************************************!*\
  !*** ./application/observer/Observer.js ***!
  \******************************************/
/*! exports provided: Observable, Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observable\", function() { return Observable; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Observer\", function() { return Observer; });\nfunction Observable(){\n  // Создаем список подписаных обьектов\n  const observers = [];\n  // Оповещение всех подписчиков о сообщении\n  this.sendMessage = function( msg ){\n      observers.map( ( obs ) => {\n        obs.notify(msg);\n      });\n  };\n  // Добавим наблюдателя\n  this.addObserver = function( observer ){\n    observers.push( observer );\n  };\n}\n\n// Сам наблюдатель:\nfunction Observer( behavior ){\n  // Делаем функцию, что бы через callback можно\n  // было использовать различные функции внутри\n  this.notify = function( msg ){\n    behavior( msg );\n  };\n}\n\n\n\n\n\n\n//# sourceURL=webpack:///./application/observer/Observer.js?");

/***/ })

/******/ });