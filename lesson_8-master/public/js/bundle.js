/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _testing_calculator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./testing/calculator */ \"./application/testing/calculator.js\");\n\n\nObject(_testing_calculator__WEBPACK_IMPORTED_MODULE_0__[\"execCalculator\"])()\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/testing/calculator.js":
/*!*******************************************!*\
  !*** ./application/testing/calculator.js ***!
  \*******************************************/
/*! exports provided: AddCommand, SubCommand, MultiplyCommand, DivideCommand, execCalculator, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AddCommand\", function() { return AddCommand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SubCommand\", function() { return SubCommand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MultiplyCommand\", function() { return MultiplyCommand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DivideCommand\", function() { return DivideCommand; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"execCalculator\", function() { return execCalculator; });\n  class Calc {\n    constructor(){\n      this.currentValue = 0;\n      this.history = [];\n    }\n    execute( command ){\n      this.currentValue = command.do( this.currentValue );\n      this.history.push(command);\n    }\n\n    undo(){\n    const cmd = this.history.pop();\n    this.currentValue = cmd.undo( this.currentValue );\n    }\n\n    getCurrentValue(){\n      console.log('CurrentValue is:', this.currentValue);\n      return( this.currentValue );\n    }\n  }\n\n  function Command(fn, undo, value) {\n      this.do = fn;\n      this.undo = undo;\n      this.value = value;\n  }\n\n function add(value){\n    return value + this.value;\n  }\n\n function sub(value){\n    return value - this.value;\n  }\n\n function multiply(value){\n  return value * this.value;\n }\n\n function divide(value){\n  return value / this.value;\n }\n\nfunction AddCommand(value){\n    Command.call( this, add, sub, value);\n  }\nfunction SubCommand(value){\n    Command.call( this, sub, add, value);\n  }\nfunction MultiplyCommand(value){\n  Command.call( this, multiply, divide, value);\n}\nfunction DivideCommand(value){\n  Command.call( this, divide, multiply, value);\n}\n\nlet Calculator = new Calc();\n\nconst execCalculator = () => {\n  Calculator.execute( new AddCommand( 20 ) );\n  Calculator.execute( new DivideCommand( 2 ) );\n  Calculator.execute( new MultiplyCommand( 5 ) );\n  Calculator.execute( new AddCommand( 30 ) );\n  Calculator.execute( new SubCommand( 10 ) );\n\n  console.log(\"after last subtraction\", Calculator.getCurrentValue());\n  Calculator.undo();\n  console.log(\"after undo\", Calculator.getCurrentValue());\n\n  Calculator.execute( new AddCommand( 3 ) );\n  Calculator.execute( new AddCommand( 16 ) );\n  Calculator.undo();\n  Calculator.execute( new SubCommand( 10 ) );\n  Calculator.getCurrentValue();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Calc);\n\n\n//# sourceURL=webpack:///./application/testing/calculator.js?");

/***/ })

/******/ });