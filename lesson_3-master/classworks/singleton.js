/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

    const _data = {
            laws: [
            {
              id: 0,
              text: '123123'
            }
          ],
            budget: 1000000,
            citizensSatisfactions: 0,
        }

    const Government = {
      addLaw: (id,name,description) => {
        _data.laws.push({"id":id,"name":name,"description":description});
        _data.citizensSatisfactions = _data.citizensSatisfactions - 10;
        console.log(_data);
      },
      readConstitutuon: () => {
        _data.laws.forEach( (item) => {
          console.log(item);
        });
      },

      readLaw: id => {
        console.log("Law ",id,_data.laws[id]);
      },

      showcitizensSatisfactions : () => {
         console.log("CitizensSatisfactions: ",_data.citizensSatisfactions);
      },

      showBudjet : () => {
         console.log("Budjet: ",_data.budget);
      },
      organizeHol : () => {
        _data.budget = _data.budget - 5000;
        _data.citizensSatisfactions = _data.citizensSatisfactions + 5;

         console.log("Result of holiday", _data);
      }

    };

Object.freeze(Government);

export default Government;

