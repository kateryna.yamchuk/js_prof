/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          + Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>
*/
const data = [
      {
        id: 1,
        link: "#1",
        name: "Established fact",
        description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
        image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg"
      },
      { 
        id: 2,
        link: "#2",
        name: "Many packages",
        description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
        image: "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg"
      },
      {
        id: 3,
        link: "#3",
        name: "Suffered alteration",
        description: "Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.",
        image: "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg"
      },{
        id: 4,
        link: "#4",
        name: "Discovered source",
        description: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
        image: "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg"
      },{
        id: 5,
        link: "#5",
        name: "Handful model",
        description: "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
        image: "http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg"
      },
    ];

const posts_feed = document.getElementById("posts_feed");
const Feed = []; let template;

class Post{
  constructor(title,image,description){
    this.id = Math.random()*10000;
    this.title = title;
    this.image = image;
    this.description = description;
    this.likes = 0;

    Feed.push( this );
    this.addLikes = this.addLikes.bind(this);
    this.render = this.render.bind(this);

  }

  render(){
    template = `
        <div class="post">
          <div class="post__title">${this.title}</div>
          <div class="post__image">
            <img src="${this.image}"/>
          </div>
          <div class="post__description">${this.description}</div>
          <div class="post__footer">
            <button class="post__like" data-id=${this.id}>Like! ${this.likes}</button>
          </div>
        </div>
      `;
    let findNode = document.querySelector(`.post[data-id="${this.id}"]`);

    if (findNode == null){
      let nodePost = document.createElement('div');
        nodePost.className = 'post';
        nodePost.dataset.id = this.id;
        posts_feed.appendChild(nodePost);
        nodePost.innerHTML = template; 
    } else {
      findNode.innerHTML = "";
      findNode.innerHTML = template;
    }

    const btn = document.querySelector(`.post__like[data-id="${this.id}"]`);
      btn.addEventListener('click',this.addLikes);
  }

  addLikes(){
    this.likes++;
    this.render();
  }

  changeAttributes(value, new_name){
    this[value] = new_name;
    this.render();
  }
}

class Advertisment extends Post{
  constructor(title,image, description,likes){
    super(title,image, description,likes);
    this.buyItem = this.buyItem.bind(this);
  }
  render(){
    template = `
        <div class="post">
          <h1>ADVERTISING</h1>
          <div class="post__title">${this.title}</div>
          <div class="post__image">
            <img src="${this.image}"/>
          </div>
          <div class="post__description">${this.description}</div>
          <div class="post__footer">
            <button class="post__like" data-id=${this.id}>Like! ${this.likes}</button>
            <button class="post__buy" data-id=${this.id}>Buy!</button>
          </div>
        </div>
      `;
    let findNode = document.querySelector(`.post[data-id="${this.id}"]`);

    if (findNode == null){
      let nodePost = document.createElement('div');
        nodePost.className = 'post';
        nodePost.dataset.id = this.id;
        posts_feed.appendChild(nodePost);
        nodePost.innerHTML = template; 
    } else {
      findNode.innerHTML = "";
      findNode.innerHTML = template;
    }

    const btn = document.querySelector(`.post__like[data-id="${this.id}"]`);
      btn.addEventListener('click',this.addLikes);

    const buy_btn = document.querySelector(`.post__buy[data-id="${this.id}"]`);
      buy_btn.addEventListener('click',this.buyItem);
  }

  buyItem(){
    console.log("You buy", this.title);
  }
}

new Post(
    "Established fact",
    "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
    "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. "
  );

new Post(
    "Many packages",
    "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg",
    "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy."
  );

new Advertisment(
    "WO WO WO",
    "https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png",
    "Meow, Meow, Meow, Meow, Meow"
  );

new Post(
    "Established fact",
    "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg",
    "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. "
  );

new Post(
    "Many packages",
    "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg",
    "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy."
  );

new Advertisment(
    "LA LA LA",
    "https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png",
    "Meow, Meow, Meow, Meow, Meow"
  );

Feed.forEach( item => {
  item.render();
})

//Feed[0].changeAttributes("title","New title for this post");
//Feed[1].changeAttributes("description","New description for this post");
