/*

  Задание:
    1. Используя функциональный декоратор, написать декоратор который будет показывать
       аргументы и результат выполнения функции.

    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,
       если они переданы строкой, и выводить ошибку если переданая переменная не
       может быть преобразована в число
*/

function Decorator1( {target, key, descriptor} ) {
  console.log('target', target, 'key', key, 'descriptor', descriptor);
  const originFunc = descriptor.value;
  
  descriptor.value = function(...args){
    console.log("arguments of",key,...args);
    let value = originFunc(...args);
    console.log("result of",key,value);
  }
}

function Decorator2( {target, key, descriptor} ) {
  console.log('target', target, 'key', key, 'descriptor', descriptor);
}

const Work1 = () => {
  
  @Decorator2
  class CoolMath {
    
  @Decorator1
    addNumbers(a,b){ return a+b; }

  @Decorator1
    multiplyNumbers(a,b){ return a*b}

  @Decorator1
    minusNumbers(a,b){ return a-b }
  }
  let Calcul = new CoolMath();

  let x = Calcul.addNumbers(2, 2)
  let y = Calcul.multiplyNumbers("10", "2")
  let z = Calcul.minusNumbers(10, 2)

};

export default Work1;
