
/*
  Повторить задание с оповещаниями (application/DecoratorExample), с
  использованием нескольких уровней абстракций, а именно паттерны:
  Decorator, Observer, Fabric

  Задача: Написать динамичную систему оповещений, которая будет отправлять
  сообщения все подписаным на неё "Мессенджерам".
  Картинки мессенджеров есть в папке public/images

  Класс оповещения должен иметь декоратор на каждый мессенджер.

  При создании обьекта класса Application нужно передавать обьект
  в котором будут находится те "Мессенджеры" который в результате будут
  подписаны на этот блок приложения.

  Отправка сообщения по "мессенджерам" должна происходить при помощи
  паттерна Observer.

  При отправке сообщения нужно создавать обьект соответствующего класса,
  для каждого типа оповещания.

  let header = new Application('slack', 'viber', 'telegramm');
  let feedback = new Application('skype', 'messanger', 'mail', telegram);

  btn.addEventListener('click', () => header.sendMessage(msg) );

  Архитектура:
  Application( messanges ) ->
    notfier = new Notifier
    renderInterface(){...}
  Notifier ->
    constructor() ->
      Fabric-> Фабрикой перебираете все типы месенджеров которые
      подписаны на эту Application;
    send() -> Отправляет сообщение всем подписчикам

*/

class Application{
  constructor(messangers){
    this.messangers = messangers;
    this.notifier = new BaseDecorator( messangers );
    this.createInterface = this.createInterface.bind(this);
  }

  createInterface(){
    const root = document.getElementById('root');
    const AppNode = document.createElement('section');

    AppNode.className = 'notifer_app';
    AppNode.innerHTML =
    `
      <div class="notifer_app--container">
        <header>
          <input class="notifier__messanger" type="text"/>
          <button class="notifier__send">Send Message</button>
        </header>
        <div class="notifier__container">
        ${
          this.messangers.map( item =>
            `
            <div class="notifier__item" data-slug="${item.name}">
              <header class="notifier__header">
                <img width="25" src="${item.image}"/>
                <span>${item.name}</span>
              </header>
              <div class="notifier__messages"></div>
            </div>
            `).join('')
        }
        </div>
      </div>
    `;
    const container = AppNode.querySelector('.notifier__container');
    const btn = AppNode.querySelector('.notifier__send');
    const input = AppNode.querySelector('.notifier__messanger');
    btn.addEventListener('click', () => {
      let value = input.value;
      this.notifier.sendMessage(value, this.node);
    });

    this.node = AppNode;
    root.appendChild(AppNode);
  }

}





let messangers = [
      {name: 'sms', image: '../public/images/sms.svg'},
      {name: 'mail', image: '../public/images/gmail.svg'},
      {name: 'telegram', image: '../public/images/telegram.svg'},
      {name: 'viber', image: '../public/images/viber.svg'},
      {name: 'slack', image: '../public/images/slack.svg'},
    ];

class Notifier {
  send( msg, baseNode, block ){
      console.log('CLASS NOTIFIER: mesasge was sended:', msg );
      const target = baseNode.querySelector(`.notifier__item[data-slug="${block}"]`);
      target.innerHTML += `<div>${msg}</div>`;
  }
}

 class SmsNotifier extends Notifier {
  send( msg, baseNode, block = 'sms' ){
    super.send(msg, baseNode, block);
  }
}

 class ViberNotifier extends Notifier {
  send( msg, baseNode, block = 'viber'){
      super.send(msg, baseNode, block);
  }
}

 class GmailNotifier extends Notifier {
  send( msg, baseNode, block = 'mail' ){
      super.send(msg, baseNode, block);
  }
}

 class TelegramNotifier extends Notifier {
  send( msg, baseNode, block = 'telegram' ){
      super.send(msg, baseNode, block);
  }
}

 class SlackNotifier extends Notifier {
  send( msg, baseNode, block = 'slack' ){
      super.send(msg, baseNode, block);
  }
}

class BaseDecorator {
  constructor( clients ){
    let obs = clients.map( obs => {
      if( obs.name === 'sms' ){
        return new SmsNotifier(obs);
      } else if( obs.name === 'mail'){
        return new GmailNotifier(obs);
      } else if( obs.name === 'telegram'){
        return new TelegramNotifier(obs);
      } else if( obs.name === 'viber'){
        return new ViberNotifier(obs);
      } else if( obs.name === 'slack'){
        return new SlackNotifier(obs);
      }
    })
    this.clients = obs;
  }
  sendMessage( msg, baseNode ){
    this.clients.map( ( obs ) => {
      obs.send(msg, baseNode);
    });
  }
}

const Demo = () => {
  let header = new Application([{name: 'mail', image: '../public/images/gmail.svg'},
                                {name: 'telegram', image: '../public/images/telegram.svg'},
                                {name: 'viber', image: '../public/images/viber.svg'}]);
  
  let feedback = new Application([{name: 'sms', image: '../public/images/sms.svg'},
                                  {name: 'slack', image: '../public/images/slack.svg'}]);
  header.createInterface();
  feedback.createInterface();
} 

export default Demo
